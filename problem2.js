let fs = require("fs");
function operationOnFile(filePath) {
     function readingfile(filePath) {
          let readingFilePromise = new Promise((resolve, reject) => {
               fs.readFile(filePath, "utf-8", (err, data) => {
                    if (err) {
                         reject(err);
                    } else {
                         resolve(data);
                    }
               });
          });
          return readingFilePromise;
     }
     function uppercase(data) {
          let uppercasePromises = new Promise((resolve, reject) => {
               let filename = "convertUpperCase.txt";
               let filePath = "/home/prasan/promises/datafile/" + filename;
               let filedata = data.toUpperCase();
               fs.writeFile(filePath, filedata, (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         storingName(filename);
                         resolve(filedata);
                    }
               });
          });
          return uppercasePromises;
     }
     function toLowercase(upperCaseFile) {
          let toLowercasePromises = new Promise((resolve, reject) => {
               let filedatalower = upperCaseFile.toLowerCase();
               let splittedFile = filedatalower.split(".").join("\n");
               let filename = "convertToLowercase.txt";
               let filePath = "/home/prasan/promises/datafile/" + filename;
               fs.writeFile(filePath, splittedFile, (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         storingName(filename);
                         resolve(filedatalower);
                    }
               });
          });
          return toLowercasePromises;
     }

     function sorting(file) {
          let sortingPromises = new Promise((resolve, reject) => {
               let sortingfile = file.split(" ");
               let sortingdata = sortingfile.sort();
               let filename = "sortedFiles.txt";
               let filePathWithName = `/home/prasan/promises/datafile/${filename}`;
               fs.writeFile(filePathWithName, sortingdata, (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         storingName(filename);
                         resolve();
                    }
               });
          });
          return sortingPromises;
     }

     function storingName(filename1) {
          let fileNames = "filenames.txt";
          let filePath = `/home/prasan/promises/${fileNames}`;
          let content = `${filename1} `;
          fs.appendFile(filePath, content, (err) => {
               if (err) {
                    console.log(err);
               } else {
                    console.log(" file has been created succefully");
               }
          });
     }
     function deletingfilesAndRead() {
          let deletingPromise = new Promise((resolve, reject) => {
               fs.readFile("/home/prasan/promises/filenames.txt","utf-8",
                    (err, data) => {
                         if (err) {
                              console.log(err);
                         } else {
                              let filePath = data.split(" ");
                              let deleted = filePath.filter((filename) => {
                                        filename.trim();
                                        return filename !== "";
                                   })
                                   .map((deletingfile) => {
                                        let fileNameToBeDeleted = "/home/prasan/promises/datafile/" + deletingfile;
                                        fs.unlink(fileNameToBeDeleted,
                                             (err) => {
                                                  if (err) {
                                                       reject(err);
                                                  } else {
                                                       resolve();
                                                  }
                                             }
                                        );
                                   });
                         }
                    }
               );
          });
     }

     readingfile(filePath)
          .then((data) => {
               console.log(data);
               return uppercase(data);
          })
          .then((uppercasedata) => {
               console.log("file has been converted to uppercase");
               return toLowercase(uppercasedata);
          })
          .then((SecondResult) => {
               console.log("file has been created to lowecase");
               return sorting(SecondResult);
          })
          .then(() => {
               console.log("sorted file created successfully");
               setTimeout(() => {
                    deletingfilesAndRead();
               }, 2000);
          })
          .then(() => {
               console.log("file has been deleted successfully");
          })
          .catch((err) => {
               console.log(err);
          });
}

module.exports = operationOnFile;
