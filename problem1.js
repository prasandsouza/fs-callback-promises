let fs = require("fs");
let path = require("path");

function createDeletefile(directory, countfile) {
     function makingdirectory(directory) {
          let directoryPromise = new Promise((resolve, reject) => {
               let directoryPath = "../" + directory;
               fs.mkdir(directoryPath, (err) => {
                    if (err) {
                         reject(err);
                    } else {
                         resolve("Directory has been created");
                    }
               });
          });
          return directoryPromise;
     }

     function creatingfile(directory, countfile) {
          let creatingFileinPromise = new Promise((resolve, reject) => {
               let count = 1;
               let intervalId = setInterval(() => {
                    let filename =
                         "../" + directory + "/" + "file" + count + ".json";
                    let filePath = path.join(
                         __dirname,
                         directory,
                         "/",
                         filename
                    );
                    if (count === countfile + 1) {
                         clearInterval(intervalId);
                    } else {
                         fs.writeFile(filePath, `${filename}`, function (err) {
                              if (err) {
                                   reject(err);
                              } else {
                                   resolve("file created successfully");
                              }
                         });
                    }
                    count += 1;
               }, 1);
          });
          return creatingFileinPromise;
     }

     function deletingfile(directoryName) {
          let deleteFileInPromises = new Promise((resolve, reject) => {
               let pathData = path.join(__dirname, directoryName);

               fs.readdir(pathData, (err, data) => {
                    if (err) {
                         console.log(err);
                    } else {
                         let arraydata = data.map((eachfile) => {
                              let pathdata = directoryName + "/" + eachfile;
                              let filepath = path.join(__dirname, pathdata);
                              fs.unlink(filepath, (err) => {
                                   if (err) {
                                        reject(err);
                                   } else {
                                        resolve("file deleted successfully");
                                   }
                              });
                              return true;
                         });
                    }
               });
          });
          return deleteFileInPromises;
     }

     makingdirectory(directory)
          .then((firstResult) => {
               console.log(firstResult);
               return creatingfile(directory, countfile);
          })
          .then((secondResult) => {
               console.log(secondResult);
               setTimeout(() => {
                    deletingfile(directory);
               }, 5000);
               return deletingfile(directory);
          })
          .then((thirdResult) => {
               setTimeout(() => {
                    console.log(thirdResult);
               }, 2000);
          })
          .catch((err) => {
               console.log(err);
          });
}
module.exports = createDeletefile;
